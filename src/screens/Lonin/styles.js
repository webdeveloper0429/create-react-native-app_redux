import {StyleSheet} from 'react-native';
import {Percent, Constants, Colors} from '../Constants';

export default StyleSheet.create({
    logoView:{
        paddingVertical: Percent.width(3),
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo:{
        color: Colors.yellow,
        fontSize: Percent.fontSize(80),
        fontWeight: 'bold'
    },
    main:{
        flex: 1,
        marginTop: Percent.width(10),
    },
    footer:{
        paddingVertical: Percent.width(5)
    }
});
