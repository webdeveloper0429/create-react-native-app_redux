import React from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import globalStyles from '../globalStyles';
import styles from './styles';
import {Colors} from '../Constants';
import MyStatusBar from '../../components/MyStatusBar';

export default class Login extends React.Component {
    static navigationOptions = {
        header: false
    }
    constructor(props){
        super(props);
        this.onLogin = this.onLogin.bind(this)
        this.state={
            username: '',
            password: ''
        }
    }
    inputUpdate(field, value){
        this.state[field] = value;
        this.setState({...this.state});
    }
    onLogin(){

    }
    render() {
        return (
            <View style={globalStyles.container}>
                <MyStatusBar backgroundColor="white"/>
                <View style={styles.logoView}>
                    <Text style={styles.logo} >eatplus</Text>
                </View>
                <View style={styles.main} >
                    <View style={globalStyles.formGroup} >
                        <TextInput
                            underlineColorAndroid='transparent'
                            style={globalStyles.textInput}
                            placeholder='USER NAME'
                            placeholderTextColor={Colors.yellow}
                            value={this.state.username}
                            onChangeText={value=> this.inputUpdate('username', value)}
                        />
                    </View>
                    <View style={globalStyles.formGroup} >
                        <TextInput
                            underlineColorAndroid='transparent'
                            style={globalStyles.textInput}
                            placeholder='PASSWORD'
                            placeholderTextColor={Colors.yellow}
                            value={this.state.password}
                            onChangeText={value=> this.inputUpdate('password', value)}
                        />
                    </View>
                </View>
                <View style={styles.footer} >
                    <View style={globalStyles.formGroup} >
                        <TouchableOpacity onPress={this.onLogin} style={globalStyles.yellowButton} >
                            <Text style={globalStyles.btnTextWhite} >Login</Text>
                        </TouchableOpacity>
                    </View>                    
                    <View style={globalStyles.formGroup} >
                        <TouchableOpacity onPress={this.onLogin} style={globalStyles.yellowButton} >
                            <Text style={globalStyles.btnTextWhite} >Register</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
