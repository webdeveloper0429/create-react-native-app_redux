import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { StatusBar, Platform } from 'react-native';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export const Constants = {
    Statusbar_Height : STATUSBAR_HEIGHT
}
export const Colors = {
    yellow : '#F89829'
}

export const Percent={
    width: (percent)=>{
        return responsiveWidth(percent);
    },
    height: (percent)=>{
        return responsiveHeight(percent);
    },
    fontSize: (num)=>{
        return responsiveFontSize(num/10);
    }
}
