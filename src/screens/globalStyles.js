import {StyleSheet} from 'react-native';
import {Percent, Constants, Colors} from './Constants';

export default StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white',
    },
    statusBar:{
        height: Constants.Statusbar_Height
    },
    main:{
        flex: 1,
    },
    formGroup: {
        paddingHorizontal: Percent.width(10),
        paddingVertical: Percent.width(2)
    },
    textInput:{
        paddingHorizontal: Percent.width(3),
        backgroundColor: '#F4F4F4',
        fontSize: Percent.fontSize(25),
        height: Percent.width(12),
        borderRadius: Percent.width(2)
    },
    yellowButton:{
        height: Percent.width(12),
        backgroundColor: Colors.yellow,
        borderRadius: Percent.width(2),
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnTextWhite:{
        color: 'white',
        fontSize: Percent.fontSize(30),
        fontWeight: 'bold'
    }
})
