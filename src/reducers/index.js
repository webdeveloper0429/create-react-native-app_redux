import { combineReducers } from 'redux';
import isLogin from './isLogin';

const reducer = combineReducers({
    isLogin,
})

export default reducer