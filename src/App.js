import React, { Component } from 'react';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

import logger from 'redux-logger';
import reducer from './reducers';

import {addNavigationHelpers, StackNavigator } from 'react-navigation'
import Login from './screens/Lonin/Login';


const store = createStore(reducer, {}, compose(applyMiddleware(logger), window.devToolsExtension  ? window.devToolsExtension() : f => f));


export const EatPlusManager = StackNavigator({
    Login: { screen: Login }
})

export default class App extends Component {
    render(){
        return(
            <Provider store={store}>
                <EatPlusManager />
            </Provider>
        )
    }
}