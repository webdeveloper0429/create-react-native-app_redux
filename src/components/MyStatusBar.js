import React, {Component} from 'react';
import { View, StyleSheet, StatusBar} from 'react-native';
import globalStyles from '../screens/globalStyles';
import {Percent} from "../screens/Constants"

const MyStatusBar = ({ backgroundColor, ...props }) => (
    <View style={[globalStyles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

const styles = StyleSheet.create({
});

export default MyStatusBar;